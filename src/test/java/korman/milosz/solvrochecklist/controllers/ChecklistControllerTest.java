package korman.milosz.solvrochecklist.controllers;

import korman.milosz.solvrochecklist.converters.ChecklistSetToStringSetConverter;
import korman.milosz.solvrochecklist.converters.SpecialCharStringToSpaceString;
import korman.milosz.solvrochecklist.converters.StringToChecklistConverter;
import korman.milosz.solvrochecklist.domain.Checklist;
import korman.milosz.solvrochecklist.services.ChecklistService;
import korman.milosz.solvrochecklist.services.DuplicateException;
import korman.milosz.solvrochecklist.services.ResourceNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Set;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.anySetOf;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class ChecklistControllerTest {

    @Mock
    ChecklistService service;

    @Mock
    StringToChecklistConverter stringToChecklistConverter;

    @Mock
    ChecklistSetToStringSetConverter checklistSetToStringSetConverter;

    @Mock
    SpecialCharStringToSpaceString specialToSpace;

    @InjectMocks
    ChecklistController controller;

    MockMvc mockMvc;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        mockMvc = MockMvcBuilders.standaloneSetup(controller)
                .setControllerAdvice(new RestResponseEntityExceptionHandler())
                .build();
    }

    @Test
    public void getChecklists() throws Exception {
        //given
        Checklist checklist1 = new Checklist();
        checklist1.setId(1L);
        checklist1.setName("Checklist 1");
        Checklist checklist2 = new Checklist();
        checklist2.setId(2L);
        checklist2.setName("Checklist 2");

        when(service.getChecklists()).thenReturn(Set.of(checklist1, checklist2));
        when(checklistSetToStringSetConverter.convert(anySetOf(Checklist.class))).thenReturn(Set.of(checklist1.getName(), checklist2.getName()));

        //when//then
        mockMvc.perform(get(ChecklistController.BASE_URL)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)));
    }

    @Test
    public void postChecklistHappyPath() throws Exception {
        final String CHECKLIST_NAME = "Checklist name";
        final Checklist CHECKLIST = new Checklist(CHECKLIST_NAME);
        final Checklist CHECKLIST_RETURNED = new Checklist(CHECKLIST_NAME);
        CHECKLIST_RETURNED.setId(1L);

        //given
        when(stringToChecklistConverter.convert(CHECKLIST_NAME)).thenReturn(CHECKLIST);
        when(service.saveChecklist(CHECKLIST)).thenReturn(CHECKLIST_RETURNED);

        //when//then
        mockMvc.perform(post(ChecklistController.BASE_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(CHECKLIST_NAME))
                .andExpect(status().isCreated());
    }

    @Test
    public void postChecklistIncorrectBody() throws Exception {
        //given//when//then
        mockMvc.perform(post(ChecklistController.BASE_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(""))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void postChecklistDuplicate() throws Exception {
        final String CHECKLIST_NAME = "Checklist name";
        final Checklist CHECKLIST = new Checklist(CHECKLIST_NAME);

        //given
        when(service.saveChecklist(CHECKLIST)).thenThrow(DuplicateException.class);
        when(stringToChecklistConverter.convert(CHECKLIST_NAME)).thenReturn(CHECKLIST);

        //when//then
        mockMvc.perform(post(ChecklistController.BASE_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(CHECKLIST_NAME))
                .andExpect(status().isConflict());
    }

    @Test
    public void deleteChecklistExistent() throws Exception {
        final String CHECKLIST_NAME = "Checklist^name";

        //given
        when(specialToSpace.convert(CHECKLIST_NAME)).thenReturn(CHECKLIST_NAME);

        //when//then
        mockMvc.perform(delete(ChecklistController.BASE_URL + "/" + CHECKLIST_NAME))
                .andExpect(status().isOk());
    }

    @Test
    public void deleteChecklistNonExistent() throws Exception {
        final String CHECKLIST_NAME = "Checklist^name";

        //given//when
        doThrow(ResourceNotFoundException.class).when(service).deleteByName(anyString());
        when(specialToSpace.convert(CHECKLIST_NAME)).thenReturn(CHECKLIST_NAME);

        //then
        mockMvc.perform(delete(ChecklistController.BASE_URL + "/" + CHECKLIST_NAME))
                .andExpect(status().isNotFound());
    }

}