package korman.milosz.solvrochecklist.controllers;

import korman.milosz.solvrochecklist.converters.ItemSetToItemDTOSetConverter;
import korman.milosz.solvrochecklist.converters.SpecialCharStringToSpaceString;
import korman.milosz.solvrochecklist.converters.StringToItemConverter;
import korman.milosz.solvrochecklist.domain.Item;
import korman.milosz.solvrochecklist.dtos.ItemDTO;
import korman.milosz.solvrochecklist.services.DuplicateException;
import korman.milosz.solvrochecklist.services.ItemService;
import korman.milosz.solvrochecklist.services.ResourceNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Set;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class ItemControllerTest {

    final String CHECKLIST_NAME = "Checklist^name";

    final String ITEM_NAME = "Item name";

    @Mock
    ItemService service;

    @Mock
    ItemSetToItemDTOSetConverter iSetToIDTOSetConverter;

    @Mock
    StringToItemConverter stringToItemConverter;

    @Mock
    SpecialCharStringToSpaceString specialToSpace;

    @InjectMocks
    ItemController controller;

    MockMvc mockMvc;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        mockMvc = MockMvcBuilders.standaloneSetup(controller)
                .setControllerAdvice(new RestResponseEntityExceptionHandler())
                .build();
    }

    @Test
    public void getItemsChecklistExistent() throws Exception {
        Item item1 = new Item(1L, "Item 1", false, null);
        Item item2 = new Item(2L, "Item 2", true, null);
        ItemDTO itemDTO1 = new ItemDTO("Item 1", false);
        ItemDTO itemDTO2 = new ItemDTO("Item 2", true);

        //given
        when(service.getItemsByChecklistName(CHECKLIST_NAME)).thenReturn(Set.of(item1, item2));
        when(iSetToIDTOSetConverter.convert(anySet())).thenReturn(Set.of(itemDTO1, itemDTO2));
        when(specialToSpace.convert(CHECKLIST_NAME)).thenReturn(CHECKLIST_NAME);

        //when//then
        mockMvc.perform(get(ChecklistController.BASE_URL + "/" + CHECKLIST_NAME + "/items")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)));
    }

    @Test
    public void getItemsChecklistNonExistent() throws Exception {
        //given
        when(service.getItemsByChecklistName(anyString())).thenThrow(ResourceNotFoundException.class);
        when(specialToSpace.convert(CHECKLIST_NAME)).thenReturn(CHECKLIST_NAME);

        //when//then
        mockMvc.perform(get(ChecklistController.BASE_URL + "/" + CHECKLIST_NAME + "/items"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void postItemChecklistExistent() throws Exception {
        //given
        Item toReturn = new Item(1L, ITEM_NAME, false, null);
        Item converted = new Item(null, ITEM_NAME, false, null);
        when(stringToItemConverter.convert(ITEM_NAME)).thenReturn(converted);
        when(service.saveItemByChecklistName(anyString(), any(Item.class))).thenReturn(toReturn);
        when(specialToSpace.convert(CHECKLIST_NAME)).thenReturn(CHECKLIST_NAME);

        //when//then
        mockMvc.perform(post(ChecklistController.BASE_URL + "/" + CHECKLIST_NAME + "/items")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(ITEM_NAME))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$", equalTo(1)));
    }

    @Test
    public void postItemChecklistNonExistent() throws Exception {
        //given
        Item converted = new Item(null, ITEM_NAME, false, null);
        when(stringToItemConverter.convert(ITEM_NAME)).thenReturn(converted);
        when(service.saveItemByChecklistName(anyString(), any(Item.class))).thenThrow(ResourceNotFoundException.class);
        when(specialToSpace.convert(CHECKLIST_NAME)).thenReturn(CHECKLIST_NAME);

        //when//then
        mockMvc.perform(post(ChecklistController.BASE_URL + "/" + CHECKLIST_NAME + "/items")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(ITEM_NAME))
                .andExpect(status().isNotFound());
    }

    @Test
    public void postItemDuplicate() throws Exception {
        //given
        Item converted = new Item(null, ITEM_NAME, false, null);
        when(stringToItemConverter.convert(ITEM_NAME)).thenReturn(converted);
        when(service.saveItemByChecklistName(anyString(), any(Item.class))).thenThrow(DuplicateException.class);
        when(specialToSpace.convert(CHECKLIST_NAME)).thenReturn(CHECKLIST_NAME);

        //when//then
        mockMvc.perform(post(ChecklistController.BASE_URL + "/" + CHECKLIST_NAME + "/items")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(ITEM_NAME))
                .andExpect(status().isConflict());
    }

    @Test
    public void patchItemExistent() throws Exception {
        //given
        when(specialToSpace.convert(CHECKLIST_NAME)).thenReturn(CHECKLIST_NAME);

        //when//then
        mockMvc.perform(patch(ChecklistController.BASE_URL + "/" + CHECKLIST_NAME + "/items" + "/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content("true"))
                .andExpect(status().isAccepted());
    }

    @Test
    public void patchItemNonExistent() throws Exception {
        //given
        when(service.updateItemByChecklistNameAndId(anyString(), any(Item.class)))
                .thenThrow(ResourceNotFoundException.class);
        when(specialToSpace.convert(CHECKLIST_NAME)).thenReturn(CHECKLIST_NAME);

        //when//then
        mockMvc.perform(patch(ChecklistController.BASE_URL + "/" + CHECKLIST_NAME + "/items" + "/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content("true"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void deleteItemExistent() throws Exception {
        //given
        when(specialToSpace.convert(CHECKLIST_NAME)).thenReturn(CHECKLIST_NAME);

        //when//then
        mockMvc.perform(delete(ChecklistController.BASE_URL + "/" + CHECKLIST_NAME + "/items" + "/1"))
                .andExpect(status().isOk());
    }

    @Test
    public void deleteItemNonExistent() throws Exception {
        //given
        doThrow(ResourceNotFoundException.class).when(service).deleteItemByChecklistNameAndId(anyString(), anyLong());
        when(specialToSpace.convert(CHECKLIST_NAME)).thenReturn(CHECKLIST_NAME);

        //when//then
        mockMvc.perform(delete(ChecklistController.BASE_URL + "/" + CHECKLIST_NAME + "/items" + "/1"))
                .andExpect(status().isNotFound());
    }

}
