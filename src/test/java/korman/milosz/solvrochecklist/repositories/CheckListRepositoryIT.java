package korman.milosz.solvrochecklist.repositories;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;

import korman.milosz.solvrochecklist.domain.Checklist;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.HashSet;
import java.util.Optional;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = NONE)
class CheckListRepositoryIT {

    private static final long CHECKLIST_ID = 1L;
    private static final String CHECKLIST_NAME = "Checklist";

    @Autowired
    CheckListRepository repository;

    @BeforeEach
    void setUp() {
        Checklist checklist = new Checklist(CHECKLIST_ID, CHECKLIST_NAME, new HashSet<>());
        repository.save(checklist);
    }

    @Test
    void findById() {
        //given//when
        Checklist foundChecklist = repository.findById(CHECKLIST_ID).get();

        //then
        assertEquals(Long.valueOf(CHECKLIST_ID), foundChecklist.getId());
        assertEquals(CHECKLIST_NAME, foundChecklist.getName());
    }

    @Test
    void findByIdNotPresent() {
        //given//when
        repository.deleteAll();

        //then
        assertEquals(Optional.empty(), repository.findById(2L));
    }

    @Test
    void doesAddId() {
        final String NO_ID_NAME = "Name";

        //given
        Checklist toSave = new Checklist(NO_ID_NAME);

        //when
        Checklist saved = repository.save(toSave);

        //then
        assertEquals(NO_ID_NAME, saved.getName());
        assertNotEquals(0L, saved.getId());
    }

    @Test
    void deleteByName() {
        final Long ID = 4L;
        final String NAME = "Some name";

        //given
        Checklist toDelete = new Checklist(ID, NAME, new HashSet<>());
        repository.save(toDelete);

        //when
        repository.deleteByName(NAME);

        //then
        assertEquals(Optional.empty(), repository.findById(ID));
    }

    @Test
    void updateName() {
        final String NEW_NAME = "New name";

        //given
        Checklist beforeUpdate = repository.findById(CHECKLIST_ID).get();

        //when
        beforeUpdate.setName(NEW_NAME);
        repository.save(beforeUpdate);

        //then
        Checklist afterUpdate = repository.findById(CHECKLIST_ID).get();
        assertEquals(Long.valueOf(CHECKLIST_ID), afterUpdate.getId());
        assertEquals(NEW_NAME, afterUpdate.getName());
    }
}