package korman.milosz.solvrochecklist.services;

import korman.milosz.solvrochecklist.domain.Checklist;
import korman.milosz.solvrochecklist.repositories.CheckListRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class ChecklistServiceImplTest {

    private final String CHECKLIST_NAME = "Name";

    @Mock
    CheckListRepository repository;

    ChecklistService checklistService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);

        checklistService = new ChecklistServiceImpl(repository);
    }

    @Test
    void getChecklistsEmpty() {
        //given empty repository
        when(repository.findAll()).thenReturn(Collections.emptySet());

        //when
        Set<Checklist> lists = checklistService.getChecklists();

        //then
        assertEquals(0, lists.size());
    }

    @Test
    void getChecklists() {
        final Checklist CHECKLIST1 = new Checklist(1L, "Name 1", new HashSet<>());
        final Checklist CHECKLIST2 = new Checklist(2L, "Name 2", new HashSet<>());
        final Checklist CHECKLIST3 = new Checklist(3L, "Name 3", new HashSet<>());

        //given
        when(repository.findAll()).thenReturn(List.of(CHECKLIST1, CHECKLIST2, CHECKLIST3));

        //when
        Set<Checklist> lists = checklistService.getChecklists();

        //then
        assertTrue(lists.contains(CHECKLIST1));
        assertTrue(lists.contains(CHECKLIST2));
        assertTrue(lists.contains(CHECKLIST3));
        assertEquals(3, lists.size());
    }

    @Test
    void saveNewChecklist() {
        //given
        when(repository.existsByName(CHECKLIST_NAME)).thenReturn(false);

        //when
        Checklist toSave = new Checklist(CHECKLIST_NAME);
        checklistService.saveChecklist(toSave);

        //then
        verify(repository, times(1)).existsByName(CHECKLIST_NAME);
        verify(repository, times(1)).save(toSave);
    }

    @Test
    void saveChecklistDuplicate() {
        final long ID = 1L;

        //given
        when(repository.existsByName(CHECKLIST_NAME)).thenReturn(true);

        //when
        Checklist duplicate = new Checklist(ID, CHECKLIST_NAME, new HashSet<>());

        //then
        assertThrows(DuplicateException.class, () -> checklistService.saveChecklist(duplicate));
    }

    @Test
    void deleteByNameExistent() {
        //given
        when(repository.existsByName(CHECKLIST_NAME)).thenReturn(true);

        //when
        checklistService.deleteByName(CHECKLIST_NAME);

        //then
        verify(repository, times(1)).existsByName(CHECKLIST_NAME);
        verify(repository, times(1)).deleteByName(CHECKLIST_NAME);
    }

    @Test
    void deleteByNameNonExistent() {
        //given
        when(repository.existsByName(CHECKLIST_NAME)).thenReturn(false);

        //when//then
        assertThrows(ResourceNotFoundException.class, () -> checklistService.deleteByName(CHECKLIST_NAME));
    }
}