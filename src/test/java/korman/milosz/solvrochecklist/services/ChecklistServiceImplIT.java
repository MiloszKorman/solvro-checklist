package korman.milosz.solvrochecklist.services;

import korman.milosz.solvrochecklist.domain.Checklist;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class ChecklistServiceImplIT {

    @Autowired
    ChecklistService service;

    @Test
    public void addsId() {
        final String CHECKLIST_NAME = "Checklist name";

        //given
        Checklist checklist = new Checklist(CHECKLIST_NAME);

        //when
        Checklist saved = service.saveChecklist(checklist);

        //then
        assertNotNull(saved.getId());
        assertEquals(CHECKLIST_NAME, saved.getName());
    }
}
