package korman.milosz.solvrochecklist.services;

import korman.milosz.solvrochecklist.domain.Checklist;
import korman.milosz.solvrochecklist.domain.Item;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class ItemServiceImplIT {

    @Autowired
    ChecklistService checklistService;

    @Autowired
    ItemService itemService;

    @Test
    @Transactional
    public void addsId() {
        final String CHECKLIST_NAME = "Checklist name2";
        final String ITEM_NAME = "Item name";

        //given
        Checklist checklist = new Checklist(CHECKLIST_NAME);
        checklistService.saveChecklist(checklist);

        //when
        Item item = new Item(ITEM_NAME, false);
        Item saved = itemService.saveItemByChecklistName(CHECKLIST_NAME, item);

        //then
        assertNotNull(saved.getId());
        assertEquals(ITEM_NAME, saved.getName());
        assertTrue(itemService.getItemsByChecklistName(CHECKLIST_NAME).contains(saved));
    }

}
