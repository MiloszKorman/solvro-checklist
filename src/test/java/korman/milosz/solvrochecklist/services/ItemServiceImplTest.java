package korman.milosz.solvrochecklist.services;

import korman.milosz.solvrochecklist.domain.Checklist;
import korman.milosz.solvrochecklist.domain.Item;
import korman.milosz.solvrochecklist.repositories.CheckListRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class ItemServiceImplTest {

    private final Long CHECKLIST_ID = 1L;
    private final String CHECKLIST_NAME = "Checklist name";
    private final Long ITEM_ID = 1L;
    private final String ITEM_NAME = "Item name";

    @Mock
    CheckListRepository repository;

    ItemService itemService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        itemService = new ItemServiceImpl(repository);
    }

    @Test
    void getItemsByChecklistNameExistent() {
        //given
        Checklist checklist = new Checklist(CHECKLIST_ID, CHECKLIST_NAME, Set.of());
        Item item1 = new Item(1L, "Item 1", false, checklist);
        Item item2 = new Item(2L, "Item2", true, checklist);
        Item item3 = new Item(3L, "Item 3", false, checklist);
        Set<Item> items = Set.of(item1, item2, item3);
        checklist.setItems(items);
        when(repository.findByName(CHECKLIST_NAME)).thenReturn(Optional.of(checklist));

        //when
        Set<Item> itemsInDB = itemService.getItemsByChecklistName(CHECKLIST_NAME);

        //then
        assertEquals(items, itemsInDB);
        verify(repository, times(1)).findByName(CHECKLIST_NAME);
    }

    @Test
    void getItemsByChecklistNameNonExistent() {
        //given
        when(repository.findByName(CHECKLIST_NAME)).thenReturn(Optional.empty());

        //then
        assertThrows(ResourceNotFoundException.class, () -> itemService.getItemsByChecklistName(CHECKLIST_NAME));
    }

    @Test
    void saveItemByChecklistNameExistent() {
        //given
        Item toSave = new Item(ITEM_ID, ITEM_NAME, false, null);
        Checklist checklist = new Checklist(CHECKLIST_ID, CHECKLIST_NAME, new HashSet<>());
        when(repository.findByName(CHECKLIST_NAME)).thenReturn(Optional.of(checklist));

        //when
        Item savedDBReponse = new Item(ITEM_ID, ITEM_NAME, false, checklist);
        Checklist toReturn = new Checklist(CHECKLIST_ID, CHECKLIST_NAME, Set.of(savedDBReponse));
        when(repository.save(any())).thenReturn(toReturn);
        Item saved = itemService.saveItemByChecklistName(CHECKLIST_NAME, toSave);

        //then
        assertEquals(ITEM_ID, saved.getId());
        assertEquals(ITEM_NAME, saved.getName());
        assertEquals(checklist, saved.getOwner());
    }

    @Test
    void saveItemByChecklistNameDuplicate() {
        //given
        Item toSave = new Item(ITEM_ID, ITEM_NAME, false, null);
        Checklist checklist = new Checklist(CHECKLIST_ID, CHECKLIST_NAME, new HashSet<>());
        Item inDB = new Item(14L, ITEM_NAME, true, checklist);
        checklist.setItems(Set.of(inDB));
        when(repository.findByName(CHECKLIST_NAME)).thenReturn(Optional.of(checklist));

        //when//then
        assertThrows(DuplicateException.class, () -> itemService.saveItemByChecklistName(CHECKLIST_NAME, toSave));
    }

    @Test
    void saveItemByChecklistNameNonExistent() {
        //given
        Item toSave = new Item(ITEM_ID, ITEM_NAME, false, null);
        when(repository.findByName(CHECKLIST_NAME)).thenReturn(Optional.empty());

        //when//then
        assertThrows(ResourceNotFoundException.class, () -> itemService.saveItemByChecklistName(CHECKLIST_NAME, toSave));
    }

    @Test
    void updateItemByChecklistNameAndIdBothExistent() {
        //given
        Item itemInDB = new Item(ITEM_ID, ITEM_NAME, false, null);
        Checklist checklist = new Checklist(CHECKLIST_ID, CHECKLIST_NAME, Set.of(itemInDB));
        itemInDB.setOwner(checklist);
        when(repository.findByName(CHECKLIST_NAME)).thenReturn(Optional.of(checklist));

        //when
        Item toUpdate = new Item(ITEM_ID, ITEM_NAME, true, null);
        when(repository.save(any())).thenReturn(checklist);
        Item updated = itemService.updateItemByChecklistNameAndId(CHECKLIST_NAME, toUpdate);

        //then
        assertEquals(ITEM_NAME, updated.getName());
        assertEquals(true, updated.getChecked());
        verify(repository, times(1)).findByName(CHECKLIST_NAME);
        verify(repository, times(1)).save(any());
    }

    @Test
    void updateItemByChecklistNameAndIdItemNonExistent() {
        //given
        Checklist checklist = new Checklist(CHECKLIST_ID, CHECKLIST_NAME, new HashSet<>());
        when(repository.findByName(CHECKLIST_NAME)).thenReturn(Optional.of(checklist));

        //when//then
        assertThrows(ResourceNotFoundException.class, () ->
                itemService.updateItemByChecklistNameAndId(
                        CHECKLIST_NAME,
                        new Item(ITEM_ID, ITEM_NAME, false, null)
                )
        );
    }

    @Test
    void updateItemByChecklistNameAndIdChecklistNonExistent() {
        //given
        when(repository.findByName(CHECKLIST_NAME)).thenReturn(Optional.empty());

        //when//then
        assertThrows(ResourceNotFoundException.class, () ->
                itemService.updateItemByChecklistNameAndId(CHECKLIST_NAME, new Item()));
    }

    @Test
    void deleteItemByChecklistNameAndIdBothExistent() {
        //given
        Item inDB = new Item(ITEM_ID, ITEM_NAME, false, null);
        Set<Item> checklistsItems = new HashSet<>();
        checklistsItems.add(inDB);
        Checklist checklist = new Checklist(CHECKLIST_ID, CHECKLIST_NAME, checklistsItems);
        inDB.setOwner(checklist);
        when(repository.findByName(CHECKLIST_NAME)).thenReturn(Optional.of(checklist));

        //when
        ArgumentCaptor<Checklist> checklistCaptor = ArgumentCaptor.forClass(Checklist.class);
        itemService.deleteItemByChecklistNameAndId(CHECKLIST_NAME, ITEM_ID);

        //then
        verify(repository, times(1)).findByName(CHECKLIST_NAME);
        verify(repository, times(1)).save(checklistCaptor.capture());
        assertEquals(0, checklistCaptor.getValue().getItems().size());
    }

    @Test
    void deleteItemByChecklistNameAndIdItemNonExistent() {
        //given
        Checklist checklist = new Checklist(CHECKLIST_ID, CHECKLIST_NAME, new HashSet<>());
        when(repository.findByName(CHECKLIST_NAME)).thenReturn(Optional.of(checklist));

        //when//then
        assertThrows(ResourceNotFoundException.class, () ->
                itemService.deleteItemByChecklistNameAndId(CHECKLIST_NAME, ITEM_ID)
        );
    }

    @Test
    void deleteItemByChecklistNameAndIdChecklistNonExistent() {
        //given
        when(repository.findByName(CHECKLIST_NAME)).thenReturn(Optional.empty());

        //when//then
        assertThrows(ResourceNotFoundException.class, () ->
                itemService.deleteItemByChecklistNameAndId(CHECKLIST_NAME, 1L));
    }
}