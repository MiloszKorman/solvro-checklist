package korman.milosz.solvrochecklist.domain;

import lombok.*;

import javax.persistence.*;

@Entity
@Setter
@Getter
@EqualsAndHashCode(exclude = {"checked", "owner"})
@NoArgsConstructor
@AllArgsConstructor
public class Item {

    public Item(Long id, Boolean checked) {
        this.id = id;
        this.checked = checked;
    }

    public Item(String name, Boolean checked) {
        this.name = name;
        this.checked = checked;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private Boolean checked;

    @ManyToOne
    private Checklist owner;
}
