package korman.milosz.solvrochecklist.domain;

import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@EqualsAndHashCode(exclude = {"id", "items"})
@NoArgsConstructor
@AllArgsConstructor
public class Checklist {

    public Checklist(String name) {
        this.name = name;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true)
    private String name;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "owner")
    private Set<Item> items = new HashSet<>();

    public void addItem(Item item) {
        items.add(item);
    }
}
