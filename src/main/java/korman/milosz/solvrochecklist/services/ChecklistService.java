package korman.milosz.solvrochecklist.services;

import korman.milosz.solvrochecklist.domain.Checklist;

import java.util.Set;

public interface ChecklistService {

    Set<Checklist> getChecklists();

    Checklist saveChecklist(Checklist checklistToSave);

    void deleteByName(String nameToDelete);
}
