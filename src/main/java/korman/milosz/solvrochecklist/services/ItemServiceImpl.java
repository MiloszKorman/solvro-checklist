package korman.milosz.solvrochecklist.services;

import korman.milosz.solvrochecklist.domain.Checklist;
import korman.milosz.solvrochecklist.domain.Item;
import korman.milosz.solvrochecklist.repositories.CheckListRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.Set;

@Service
@AllArgsConstructor
public class ItemServiceImpl implements ItemService {

    private final CheckListRepository repository;

    @Override
    @Transactional(readOnly = true)
    public Set<Item> getItemsByChecklistName(String checklistName) {
        Optional<Checklist> checklistOptional = repository.findByName(checklistName);
        if (checklistOptional.isPresent()) {
            Checklist checklist = checklistOptional.get();
            return checklist.getItems();
        } else {
            throw new ResourceNotFoundException();
        }
    }

    @Override
    @Transactional
    public Item saveItemByChecklistName(String checklistName, Item itemToSave) {
        Optional<Checklist> checklistOptional = repository.findByName(checklistName);
        if (checklistOptional.isPresent()) {
            Checklist checklist = checklistOptional.get();
            //uniqueness of item by its name (within a checklist) is assumed
            if (checklist.getItems().stream().anyMatch(item -> item.getName().equals(itemToSave.getName()))) {
                throw new DuplicateException();
            } else {
                checklist.addItem(itemToSave);
                itemToSave.setOwner(checklist);
                Checklist savedChecklist = repository.save(checklist);
                return savedChecklist.getItems().stream()
                        .filter(item -> item.getName().equals(itemToSave.getName()))
                        .findFirst().get();
            }
        } else {
            throw new ResourceNotFoundException();
        }
    }

    @Override
    @Transactional
    public Item updateItemByChecklistNameAndId(String checklistName, Item itemToUpdate) {
        Optional<Checklist> checklistOptional = repository.findByName(checklistName);
        if (checklistOptional.isPresent()) {
            Checklist checklist = checklistOptional.get();
            Optional<Item> optionalToBeUpdated = checklist.getItems().stream()
                    .filter(item -> item.getId().equals(itemToUpdate.getId())).findFirst();

            if (optionalToBeUpdated.isPresent()) {
                Item toBeUpdated = optionalToBeUpdated.get();
                toBeUpdated.setOwner(checklist);
                toBeUpdated.setChecked(itemToUpdate.getChecked());
                return repository.save(checklist).getItems().stream()
                        .filter(item -> item.getId().equals(itemToUpdate.getId())).findFirst().get();
            } else {
                throw new ResourceNotFoundException();
            }
        } else {
            throw new ResourceNotFoundException();
        }
    }

    @Override
    @Transactional
    public void deleteItemByChecklistNameAndId(String checklistName, Long itemId) {
        Optional<Checklist> checklistOptional = repository.findByName(checklistName);
        if (checklistOptional.isPresent()) {
            Checklist checklist = checklistOptional.get();
            Optional<Item> optionalToDelete = checklist.getItems().stream()
                    .filter(item -> item.getId().equals(itemId)).findFirst();

            if (optionalToDelete.isPresent()) {
                Item toDelete = optionalToDelete.get();
                toDelete.setOwner(null);
                checklist.getItems().remove(toDelete);
                repository.save(checklist);
            } else {
                throw new ResourceNotFoundException();
            }
        } else {
            throw new ResourceNotFoundException();
        }
    }
}
