package korman.milosz.solvrochecklist.services;

import korman.milosz.solvrochecklist.domain.Item;

import java.util.Set;

public interface ItemService {

    Set<Item> getItemsByChecklistName(String checklistName);

    Item saveItemByChecklistName(String checklistName, Item itemToSave);

    Item updateItemByChecklistNameAndId(String checklistName, Item itemToUpdate);

    void deleteItemByChecklistNameAndId(String checklistName, Long itemId);
}
