package korman.milosz.solvrochecklist.services;

import korman.milosz.solvrochecklist.domain.Checklist;
import korman.milosz.solvrochecklist.repositories.CheckListRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Set;

@Service
public class ChecklistServiceImpl implements ChecklistService {

    private final CheckListRepository repository;

    public ChecklistServiceImpl(CheckListRepository repository) {
        this.repository = repository;
    }

    @Override
    @Transactional(readOnly = true)
    public Set<Checklist> getChecklists() {
        Set<Checklist> lists = new HashSet<>();
        repository.findAll().forEach(lists::add);
        return lists;
    }

    @Override
    @Transactional
    public Checklist saveChecklist(Checklist checklistToSave) {
        if (repository.existsByName(checklistToSave.getName())) {
            throw new DuplicateException();
        } else {
            //new checklist, empty set
            //should neither have ID nor items in set
            return repository.save(checklistToSave);
        }
    }

    @Override
    @Transactional
    public void deleteByName(String nameToDelete) {
        if (repository.existsByName(nameToDelete)) {
            repository.deleteByName(nameToDelete);
        } else {
            throw new ResourceNotFoundException();
        }
    }
}
