package korman.milosz.solvrochecklist.controllers;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import korman.milosz.solvrochecklist.converters.ItemSetToItemDTOSetConverter;
import korman.milosz.solvrochecklist.converters.SpecialCharStringToSpaceString;
import korman.milosz.solvrochecklist.converters.StringToItemConverter;
import korman.milosz.solvrochecklist.domain.Item;
import korman.milosz.solvrochecklist.dtos.ItemDTO;
import korman.milosz.solvrochecklist.services.ItemService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
@RequestMapping(ItemController.BASE_URL)
@AllArgsConstructor
public class ItemController {
    public static final String BASE_URL = ChecklistController.BASE_URL + "/{name}/items";

    private final ItemService service;

    private final ItemSetToItemDTOSetConverter iSetToIDTOSetConverter;

    private final StringToItemConverter stringToItemConverter;

    private final SpecialCharStringToSpaceString specialToSpace;

    @ApiOperation(value = "Returns list of checklist items.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "JSON array of checklist's items."),
            @ApiResponse(code = 404, message = "Checklist of given name does not exist")
    })
    @GetMapping(produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public Set<ItemDTO> getItems(@PathVariable String name) {
        return iSetToIDTOSetConverter.convert(service.getItemsByChecklistName(specialToSpace.convert(name)));
    }

    @ApiOperation(value = "Inserts new unchecked item to checklist and gives it unique ID.", consumes = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Returns ID of newly added item."),
            @ApiResponse(code = 404, message = "Checklist of given name does not exist"),
            @ApiResponse(code = 409, message = "Item of given name already exists"),
            @ApiResponse(code = 400, message = "Items name was not provided")
    })
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public Long insertNewItem(@PathVariable String name, @RequestBody String itemName) {
        return service.saveItemByChecklistName(specialToSpace.convert(name), stringToItemConverter.convert(itemName)).getId();
    }

    @ApiOperation(value = "Check or uncheck checklist's item.", consumes = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 202, message = "Patched."),
            @ApiResponse(code = 404, message = "Checklist of given name does not exist"),
            @ApiResponse(code = 404, message = "Item of given ID does not exist."),
            @ApiResponse(code = 400, message = "Items status was not provided")
    })
    @PatchMapping("/{id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void changeItemStatus(@PathVariable String name, @PathVariable Long id, @RequestBody Boolean checked) {
        service.updateItemByChecklistNameAndId(specialToSpace.convert(name), new Item(id, checked));
    }

    @ApiOperation(value = "Remove checklist's item.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok."),
            @ApiResponse(code = 404, message = "Checklist of given name does not exist"),
            @ApiResponse(code = 404, message = "Item of given ID does not exist.")
    })
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void destroyItem(@PathVariable String name, @PathVariable Long id) {
        service.deleteItemByChecklistNameAndId(specialToSpace.convert(name), id);
    }

}
