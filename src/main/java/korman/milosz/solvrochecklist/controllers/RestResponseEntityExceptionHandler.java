package korman.milosz.solvrochecklist.controllers;

import korman.milosz.solvrochecklist.services.DuplicateException;
import korman.milosz.solvrochecklist.services.ResourceNotFoundException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler({ResourceNotFoundException.class})
    public ResponseEntity<Object> handleNotFoundException() {
        return new ResponseEntity<>("Resource Not Found", new HttpHeaders(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({DuplicateException.class})
    public ResponseEntity<Object> handleDuplicateException() {
        return new ResponseEntity<>("Duplicate", new HttpHeaders(), HttpStatus.CONFLICT);
    }

}
