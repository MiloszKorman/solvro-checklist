package korman.milosz.solvrochecklist.controllers;

import io.swagger.annotations.*;
import korman.milosz.solvrochecklist.converters.ChecklistSetToStringSetConverter;
import korman.milosz.solvrochecklist.converters.SpecialCharStringToSpaceString;
import korman.milosz.solvrochecklist.converters.StringToChecklistConverter;
import korman.milosz.solvrochecklist.services.ChecklistService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
@RequestMapping(ChecklistController.BASE_URL)
@AllArgsConstructor
public class ChecklistController {
    public static final String BASE_URL = "/lists";

    private final ChecklistService service;

    private final StringToChecklistConverter stringToChecklistConverter;

    private final ChecklistSetToStringSetConverter checklistSetToStringSetConverter;

    private final SpecialCharStringToSpaceString specialToSpace;

    @ApiOperation(value = "Returns list of checklists' names.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "JSON array of checklists' names.")
    })
    @GetMapping(produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public Set<String> getChecklistsList() {
        return checklistSetToStringSetConverter.convert(service.getChecklists());
    }

    @ApiOperation(value = "Inserts new checklist with a unique name.", consumes = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "New checklist inserted."),
            @ApiResponse(code = 409, message = "Checklist of given name already exists."),
            @ApiResponse(code = 400, message = "Checklists name was not provided")
    })
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void createNewChecklist(@RequestBody String checklistName) {
        service.saveChecklist(stringToChecklistConverter.convert(checklistName));
    }

    @ApiOperation(value = "Remove checklist of given name.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK."),
            @ApiResponse(code = 404, message = "Checklist of given name does not exist.")
    })
    @DeleteMapping("/{name}")
    @ResponseStatus(HttpStatus.OK)
    public void destroyChecklist(@PathVariable String name) {
        service.deleteByName(specialToSpace.convert(name));
    }
}
