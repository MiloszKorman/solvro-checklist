package korman.milosz.solvrochecklist.repositories;

import korman.milosz.solvrochecklist.domain.Checklist;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface CheckListRepository extends CrudRepository<Checklist, Long> {

    void deleteByName(String name);

    boolean existsByName(String name);

    Optional<Checklist> findByName(String name);
}
