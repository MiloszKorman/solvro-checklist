package korman.milosz.solvrochecklist.bootstrap;

import korman.milosz.solvrochecklist.domain.Checklist;
import korman.milosz.solvrochecklist.domain.Item;
import korman.milosz.solvrochecklist.services.ChecklistService;
import korman.milosz.solvrochecklist.services.ItemService;
import lombok.AllArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class Bootstrap implements CommandLineRunner {

    public static final String SHOPPING = "shopping";
    public static final String TODO = "to-do";

    private final ChecklistService checklistService;

    private final ItemService itemService;

    @Override
    public void run(String... args) throws Exception {
        loadChecklists();
        loadItems();
    }

    private void loadChecklists() {
        Checklist shopping = new Checklist(SHOPPING);
        Checklist todo = new Checklist(TODO);

        checklistService.saveChecklist(shopping);
        checklistService.saveChecklist(todo);
    }

    private void loadItems() {
        Item bread = new Item("bread", false);
        Item eggs = new Item("eggs", false);
        Item milk = new Item("milk", false);
        Item cereal = new Item("cereal", false);

        itemService.saveItemByChecklistName(SHOPPING, bread);
        itemService.saveItemByChecklistName(SHOPPING, eggs);
        itemService.saveItemByChecklistName(SHOPPING, milk);
        itemService.saveItemByChecklistName(SHOPPING, cereal);

        Item iron = new Item("iron", false);
        Item vacuum = new Item("vacuum", true);
        Item preLunch = new Item("prepare lunch", false);
        Item makeBed = new Item("make the bed", true);

        itemService.saveItemByChecklistName(TODO, iron);
        itemService.saveItemByChecklistName(TODO, vacuum);
        itemService.saveItemByChecklistName(TODO, preLunch);
        itemService.saveItemByChecklistName(TODO, makeBed);
    }
}
