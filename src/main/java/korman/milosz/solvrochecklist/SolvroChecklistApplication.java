package korman.milosz.solvrochecklist;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SolvroChecklistApplication {

    public static void main(String[] args) {
        SpringApplication.run(SolvroChecklistApplication.class, args);
    }
}
