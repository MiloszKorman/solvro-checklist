package korman.milosz.solvrochecklist.converters;

import korman.milosz.solvrochecklist.domain.Item;
import korman.milosz.solvrochecklist.dtos.ItemDTO;
import lombok.AllArgsConstructor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.Set;
import java.util.stream.Collectors;

@Component
@AllArgsConstructor
public class ItemSetToItemDTOSetConverter implements Converter<Set<Item>, Set<ItemDTO>> {

    private final ItemToItemDTOConverter itemToItemDTOConverter;

    @Override
    public Set<ItemDTO> convert(Set<Item> items) {
        return items.stream().map(itemToItemDTOConverter::convert).collect(Collectors.toSet());
    }

}
