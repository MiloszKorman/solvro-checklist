package korman.milosz.solvrochecklist.converters;

import korman.milosz.solvrochecklist.domain.Checklist;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.Set;
import java.util.stream.Collectors;

@Component
public class ChecklistSetToStringSetConverter implements Converter<Set<Checklist>, Set<String>> {
    @Override
    public Set<String> convert(Set<Checklist> checklists) {
        return checklists.stream().map(Checklist::getName).collect(Collectors.toSet());
    }
}
