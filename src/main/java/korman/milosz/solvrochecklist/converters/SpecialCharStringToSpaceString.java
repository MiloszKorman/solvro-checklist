package korman.milosz.solvrochecklist.converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class SpecialCharStringToSpaceString implements Converter<String, String> {

    @Override
    public String convert(String s) {
        return s.replace('^', ' ');
    }
}
