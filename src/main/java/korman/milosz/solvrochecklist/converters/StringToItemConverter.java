package korman.milosz.solvrochecklist.converters;

import korman.milosz.solvrochecklist.domain.Item;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class StringToItemConverter implements Converter<String, Item> {

    @Override
    public Item convert(String s) {
        return new Item(s, false);
    }
}
