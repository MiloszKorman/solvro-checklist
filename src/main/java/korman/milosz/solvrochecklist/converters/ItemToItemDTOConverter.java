package korman.milosz.solvrochecklist.converters;

import korman.milosz.solvrochecklist.domain.Item;
import korman.milosz.solvrochecklist.dtos.ItemDTO;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class ItemToItemDTOConverter implements Converter<Item, ItemDTO> {

    @Override
    public ItemDTO convert(Item item) {
        return new ItemDTO(item.getName(), item.getChecked());
    }
}
