package korman.milosz.solvrochecklist.converters;

import korman.milosz.solvrochecklist.domain.Checklist;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class StringToChecklistConverter implements Converter<String, Checklist> {

    @Override
    public Checklist convert(String s) {
            return new Checklist(s);
    }

}
