# Solvro Checklist Rest Api: Spring Boot, MariaDB, JPA, Swagger, Lombok

Restful CRUD API for a simple checklist management. Built as a [Solvro](http://www.solvro.pwr.edu.pl/ "Solvro web page") recruitment assignment.

## Requirements
+ Java
+ Maven
+ Docker

## Steps to Setup
**1. Create a MariaDB database**

Download mariadb image:
```bash
docker pull mariadb
```

Start the container: 
```bash
docker run --name solvrodb -p 3306:3306 -e MYSQL_ROOT_PASSWORD=pass -d mariadb
```

Go into bash shell in the container: 
```bash
docker exec -it solvrodb bash
```

Log into MariaDB: 
```bash
mysql -u root -p
pass
```

Create the database:
```bash
create database solvrodb;
```

Exit MariaDB: 
```bash
ctrl+z
```

Exit containers bash shell: 
```bash
ctrl+p ctrl+q
```


**2. Clone the project**
```bash
git clone https://gitlab.com/MiloszKorman/solvro-checklist.git
```


**3. Move into the project and run it**
```bash
cd solvro-checklist/
```
```bash
mvn spring-boot:run
```


### This app will start running at <http://localhost:8080> populated with some example data.

#### Not perfect swagger documentation will be available at: <http://localhost:8080/swagger-ui.html>.
